import React from 'react';
import './App.css';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.getInputValue = this.getInputValue.bind(this);
    this.clearInput = this.clearInput.bind(this);
    this.onClickSubmit = this.onClickSubmit.bind(this);
    this.state = {
      value: 0,
    }
  }
  clearInput() {
    document.getElementById("result").value = "";
  }

  getInputValue(val) {
    document.getElementById("result").value += val;
  }

  onClickSubmit() {
    let x = document.getElementById("result").value
    // eslint-disable-next-line
    let y = eval(x)
    document.getElementById("result").value = y
  }  

  render() {
    return (
      <div className="cal-controll">        
      <div className="cal-top">
        <div className="cal-header">
          <input type="text" name="text" id="result" />
        </div>
        <div className="cal-body">
          <div className="cal-inner-btn">
            <div className="row-1">
              <button name="button" onClick={() => this.clearInput()}>C</button>
            </div>
            <div className="row-2">
              <button name="7" onClick={() => this.getInputValue(7)}>7</button>
              <button name="8" onClick={() => this.getInputValue(8)}>8</button>
              <button name="9" onClick={() => this.getInputValue(9)}>9</button>
              <button name="*" onClick={() => this.getInputValue("*")}>*</button>
            </div>
            <div className="row-3">
              <button name="4" onClick={() => this.getInputValue(4)}>4</button>
              <button name="5" onClick={() => this.getInputValue(5)}>5</button>
              <button name="6" onClick={() => this.getInputValue(6)}>6</button>
              <button name="-" onClick={() => this.getInputValue("-")}>-</button>
            </div>
            <div className="row-4">
              <button name="1" onClick={() => this.getInputValue(1)}>1</button>
              <button name="2" onClick={() => this.getInputValue(2)}>2</button>
              <button name="3" onClick={() => this.getInputValue(3)}>3</button>
              <button name="+" onClick={() => this.getInputValue("+")}>+</button>
            </div>
            <div className="row-5">
              <button name="0" onClick={() => this.getInputValue(0)}>0</button>
              <button name="/" onClick={() => this.getInputValue("/")}>/</button>
              <button name="." onClick={() => this.getInputValue(".")}>.</button>
              <button name="=" onClick={this.onClickSubmit}>=</button>
            </div>
          </div>
        </div>
      </div>
      </div>
    );
  }
}

export default App;